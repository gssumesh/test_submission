FROM node:9

# Set the work directory and copy current directory content
WORKDIR /home/service
ADD ./ /home/service

# Run build
RUN apk add --no-cache --virtual .build-deps \
    make gcc g++ python linux-headers paxctl binutils-gold && \
    npm install && \
    npm run test && \
    npm run build && \
    npm prune --production && \
    npm cache clean --force && \
    rm -rf acceptance && \
    apk del .build-deps

LABEL docker_image_json="testing_image"

CMD ["npm","start"]