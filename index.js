const lib = require('./lib');

const lifespan12 = [
    {born: 1902 , died: 1991 },
    {born: 1941 , died: 1978 },
    {born: 2004 },
    {born: 1957 },
    {born: 1989 , died: 2008 },
    {born: 1909 , died: 2005 },
    {born: 1918 },
    {born: 1913 , died: 2010 },
    {born: 1979 },
    {born: 1961 , died: 2002 },
    {born: 1977 , died: 2003 },
    {born: 1909 , died: 1991 }
];

const lifespan10 = [
    {born: 1902 , died: 1991 },
    {born: 1941 , died: 1978 },
    {born: 2004 },
    {born: 1957 },
    {born: 1989 , died: 2008 },
    {born: 1909 , died: 2005 },
    {born: 1918 },
    {born: 1913 , died: 2010 },
    {born: 1979 },
    {born: 1961 , died: 2002 }
];

const lifespan3 = [
    {born: 1913 , died: 2010 },
    {born: 1979 },
    {born: 1961 , died: 2002 }
];

let tag = "LifespanWithLength:" + lifespan12.length;
console.time(tag);
console.log(lib(lifespan12));
console.timeEnd(tag);

tag = "LifespanWithLength:" + lifespan10.length;
console.time(tag);
console.log(lib(lifespan10));
console.timeEnd(tag);

tag = "LifespanWithLength:" + lifespan3.length;
console.time(tag);
console.log(lib(lifespan3));
console.timeEnd(tag);

