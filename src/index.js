const utils = require('./utils');
const _ = require('lodash');

module.exports = lifespan => {
  const fromYear = utils.getMinBornYear(lifespan) + 1;
  const toYear = utils.getMaxDiedYear(lifespan) + 1;

  return _.filter(
    _.range(fromYear, toYear + 1),
    year =>
      utils.getPopulationByYear(lifespan, year) <
      utils.getPopulationByYear(lifespan, year - 1)
  );
};
