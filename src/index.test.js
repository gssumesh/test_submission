/**
 * @file index.js
 * Contains  test for the getYearsWithLowerPopulation  in index.js.
 */

/* global test expect */

import src from '.';

describe('should have a module: ', () => {
  let inputLifespan;

  beforeAll(() => {
    inputLifespan = [
      { born: 1902, died: 1991 },
      { born: 1941, died: 1978 },
      { born: 2004 },
      { born: 1957 },
      { born: 1989, died: 2008 },
      { born: 1909, died: 2005 },
      { born: 1918 },
      { born: 1913, died: 2010 },
      { born: 1979 },
      { born: 1961, died: 2002 },
      { born: 1977, died: 2003 },
      { born: 1909, died: 1991 }
    ];
  });

  it('should export a function', () => {
    expect(typeof src).toEqual('function');
  });

  it('should return array of years within input range when population was lower than previous year', () => {
    expect(src(inputLifespan)).toEqual([1992, 2003, 2006, 2009, 2011]);
  });
});
