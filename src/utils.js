const _ = require('lodash');

const getMinBornYear = lifespan => {
  const minMember = _.minBy(lifespan, member => member.born);
  return minMember ? minMember.born : 0;
};

const getMaxDiedYear = lifespan => {
  const maxMember = _.maxBy(lifespan, member => member.died);
  return maxMember ? maxMember.died : 0;
};

const getPopulationByYear = (lifespan, year) =>
  _.reduce(
    lifespan,
    (population, member) => {
      if (member.died) {
        return year >= member.born && year <= member.died
          ? population + 1
          : population;
      }

      return year >= member.born ? population + 1 : population;
    },
    0
  );

module.exports = {
  getPopulationByYear,
  getMaxDiedYear,
  getMinBornYear
};
