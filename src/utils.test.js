/**
 * @file utils.js
 * Contains test for functional utility methods in utils.js.
 */

/* global test expect */

import utils from './utils';

describe('should have utils module: ', () => {
  describe('with method getPopulationByYear(): ', () => {
    let inputLifespan;

    beforeAll(() => {
      inputLifespan = [
        { born: 1902, died: 1991 },
        { born: 1941, died: 1978 },
        { born: 2004 },
        { born: 1957 },
        { born: 1989, died: 2008 },
        { born: 1909, died: 2005 },
        { born: 1918 },
        { born: 1913, died: 2010 },
        { born: 1979 },
        { born: 1961, died: 2002 },
        { born: 1977, died: 2003 },
        { born: 1909, died: 1991 }
      ];
    });

    it('should return population count for a specific year based on existence within input', () => {
      expect(utils.getPopulationByYear(inputLifespan, 1992)).toEqual(8);
    });

    it('should return population count as 0 for a specific year that is lower than minimum born year in input', () => {
      expect(utils.getPopulationByYear(inputLifespan, 1800)).toEqual(0);
    });

    it('should return population count as sum of all years without died entry for specific year that is higher than maximum died year in input', () => {
      expect(utils.getPopulationByYear(inputLifespan, 3000)).toEqual(4);
    });

    it('should return population count as 0 for empty input', () => {
      expect(utils.getPopulationByYear()).toEqual(0);
    });
  });

  describe('with method getMaxDiedYear(): ', () => {
    let inputLifespan;

    beforeAll(() => {
      inputLifespan = [
        { born: 1902, died: 1991 },
        { born: 1941, died: 1978 },
        { born: 2004 },
        { born: 1957 },
        { born: 1989, died: 2008 },
        { born: 1909, died: 2005 },
        { born: 1918 },
        { born: 1913, died: 2010 },
        { born: 1979 },
        { born: 1961, died: 2002 },
        { born: 1977, died: 2003 },
        { born: 1909, died: 1991 }
      ];
    });

    it('should return maximum entry in all died year in input', () => {
      expect(utils.getMaxDiedYear(inputLifespan)).toEqual(2010);
    });

    it('should return maximum entry as 0 for empty input', () => {
      expect(utils.getMaxDiedYear()).toEqual(0);
    });
  });

  describe('with method getMinBornYear(): ', () => {
    let inputLifespan;

    beforeAll(() => {
      inputLifespan = [
        { born: 1902, died: 1991 },
        { born: 1941, died: 1978 },
        { born: 2004 },
        { born: 1957 },
        { born: 1989, died: 2008 },
        { born: 1909, died: 2005 },
        { born: 1918 },
        { born: 1913, died: 2010 },
        { born: 1979 },
        { born: 1961, died: 2002 },
        { born: 1977, died: 2003 },
        { born: 1909, died: 1991 }
      ];
    });

    it('should return minimum entry in all born year in input', () => {
      expect(utils.getMinBornYear(inputLifespan)).toEqual(1902);
    });

    it('should return minimum entry as 0 for empty input', () => {
      expect(utils.getMinBornYear()).toEqual(0);
    });
  });
});
